#!/bin/sh 

echo "Installing necessary packages and Downloading the Application" &&
echo "Only the Application files are updated" &&
sleep 5 &&

pkg remove -y game-repo &&
pkg remove -y science-repo &&
export DEBIAN_FRONTEND=noninteractive &&
apt-get update && 
apt-get -o Dpkg::Options::="--force-confold" upgrade -q -y --force-yes &&
apt-get -y install git build-essential cmake proot libmicrohttpd clang python &&
git clone https://gitlab.com/mehowdude/mobile-miner-termux.git &&
chmod u+x -R mobile-miner-termux &&
mv mobile-miner-termux/start.sh start.sh 

echo "\n\n\n\n" &&
echo "Installing is Completed!!!" &&
echo "Run the start.sh to start the Application" &&
rm installer.sh
